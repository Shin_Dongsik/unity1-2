﻿using UnityEngine;

namespace Game
{
    public class UIMenu : UIWindow
    {
        [SerializeField]
        private GameObject _container;

        public void OnOptionClicked()
        {
            ToggleContainerActive();
        }

        public void OnTitleClicked()
        {
            GameSystem._Instance.LoadTitleScene();
        }

        //로그인씬 로딩
        public void OnLoginClicked()
        {
            GameSystem._Instance.LoadLoginScene();
        }

        private void ToggleContainerActive()
        {
            _container.SetActive(!_container.activeSelf);
        }
    }
}